package com.zuitt.example;

public class Variables {
    /*
    * Data Types
    * Primitive
    *   - int for integer
    *   - double for float
    *   - char for single characters
    *   - boolean for boolean values
    * Non-Primitive
    *   - String
    *   - Array
    *   - Classes
    *   - Interfaces
    * */
    public static void main(String[] args) {
        int age = 18;
        char middle_initial = 'V';
        boolean isLegalAge = true;
        System.out.println("The use age is " + age);
        System.out.println("The user middle initial is " + middle_initial);
        System.out.println("Is the user of legal age? " + isLegalAge);
    }
}
